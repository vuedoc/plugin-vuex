# Vuex Vuedoc Plugin

This adds support of Vuex on Vuedoc

[![npm](https://img.shields.io/npm/v/@vuedoc/plugin-vuex.svg)](https://www.npmjs.com/package/@vuedoc/plugin-vuex)
[![Build status](https://gitlab.com/vuedoc/plugin-vuex/badges/main/pipeline.svg)](https://gitlab.com/vuedoc/plugin-vuex/pipelines?ref=main)
[![Test coverage](https://gitlab.com/vuedoc/plugin-vuex/badges/main/coverage.svg)](https://gitlab.com/vuedoc/plugin-vuex/-/jobs)
[![Buy me a beer](https://img.shields.io/badge/Buy%20me-a%20beer-1f425f.svg)](https://www.buymeacoffee.com/demsking)


## Install

This package is [ESM only](https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c)
: Node 16+ is needed to use it and it must be imported instead of required.

```sh
npm install --save @vuedoc/plugin-vuex
```

## Usage

**Usage with Vuedoc Markdown**

```js
// vuedoc.config.js
import { Loader } from '@vuedoc/parser';
import { createVuexPlugin } from '@vuedoc/plugin-vuex';

export default {
  output: 'docs/',
  parsing: {
    plugins: [
      createVuexPlugin([
        'path/to/store.js',
      ]),
    ],
  },
};
```

Then:

```sh
vuedoc-md --config vuedoc.config.js components/*.vue
```

**Usage with Vuedoc Parser**

```js
import { parseComponent } from '@vuedoc/parser';
import { createVuexPlugin } from '@vuedoc/plugin-vuex';

const component = await parseComponent({
  filename: 'test/examples/classic/counter-hot/CounterControls.vue',
  plugins: [
    createVuexPlugin([
      'path/to/store.js',
    ]),
  ],
});
```

## Development Setup

1. [Install Nix Package Manager](https://nixos.org/manual/nix/stable/installation/installing-binary.html)

2. [Install `direnv` with your OS package manager](https://direnv.net/docs/installation.html#from-system-packages)

3. [Hook it `direnv` into your shell](https://direnv.net/docs/hook.html)

4. At the top-level of your project run:

   ```sh
   direnv allow
   ```

   > The next time your launch your terminal and enter the top-level of your
   > project, `direnv` will check for changes.

## Versioning

Given a version number `MAJOR.MINOR.PATCH`, increment the:

- `MAJOR` version when you make incompatible API changes,
- `MINOR` version when you add functionality in a backwards-compatible manner,
  and
- `PATCH` version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions
to the `MAJOR.MINOR.PATCH` format.

See [SemVer.org](https://semver.org/) for more details.

## License

Under the MIT license.
See [LICENSE](https://gitlab.com/vuedoc/plugin-vuex/blob/main/LICENSE) file for more
details.
