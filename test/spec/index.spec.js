import { ComponentTestCase } from '@vuedoc/parser/test-utils';
import { createVuexPlugin } from '../../src/index.ts';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

function resolve(filename) {
  return join(__dirname, `../examples/${filename}`);
}

ComponentTestCase({
  name: 'createStore()',
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filecontent: `
      <script setup>
        import { createStore } from 'vuex'

        const store = createStore()
      </script>
    `,
  },
  expected: {
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'store',
        type: 'Store<S>',
        initialValue: '',
        keywords: [],
        visibility: 'public' },
    ],
    props: [],
    methods: [],
  },
});

ComponentTestCase({
  name: 'createStore() with explicit typing',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filecontent: `
      <script lang="ts" setup>
        import { createStore } from 'vuex'

        type Account = {
          username: string;
        }

        const store = createStore<Account>()
      </script>
    `,
  },
  expected: {
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'store',
        type: 'Store<Account>',
        initialValue: '',
        keywords: [],
        visibility: 'public' },
    ],
    props: [],
    methods: [],
  },
});

ComponentTestCase({
  name: 'useStore()',
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filecontent: `
      <script setup>
        import { useStore } from 'vuex'

        const store = useStore()
      </script>
    `,
  },
  expected: {
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'store',
        type: 'Store<S>',
        initialValue: '',
        keywords: [],
        visibility: 'public' },
    ],
    props: [],
    methods: [],
  },
});

ComponentTestCase({
  name: 'getter & state',
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filecontent: `
      <script setup>
        import { computed } from 'vue'
        import { useStore } from 'vuex'
        
        export default {
          setup () {
            const store = useStore()
        
            return {
              // access a state in computed function
              count: computed(() => store.state.count),
        
              // access a getter in computed function
              double: computed(() => store.getters.double)
            }
          }
        }        
      </script>
    `,
  },
  expected: {
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'count',
        type: 'object',
        description: 'access a state in computed function',
        visibility: 'public',
        keywords: [],
        dependencies: [] },
      {
        kind: 'computed',
        name: 'double',
        type: 'object',
        description: 'access a getter in computed function',
        visibility: 'public',
        keywords: [],
        dependencies: [] },
    ],
    data: [],
    props: [],
    methods: [],
  },
});

ComponentTestCase({
  name: 'getter & state with typing',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filecontent: `
      <script>
        import { computed } from 'vue'
        import { useStore } from 'vuex'

        type MyState = {
          count: number;
          readonly double: number;
        }

        export default {
          setup () {
            const store = useStore<MyState>()

            return {
              // access a state in computed function
              count: computed(() => store.state.count),

              // access a getter in computed function
              double: computed(() => store.getters.double)
            }
          }
        }
      </script>
    `,
  },
  expected: {
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'count',
        type: 'number',
        description: 'access a state in computed function',
        visibility: 'public',
        keywords: [],
        dependencies: [] },
      {
        kind: 'computed',
        name: 'double',
        type: 'number',
        description: 'access a getter in computed function',
        visibility: 'public',
        keywords: [],
        dependencies: [] },
    ],
    data: [],
    props: [],
    methods: [],
  },
});

ComponentTestCase({
  name: 'Accessing Mutations and Actions',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filecontent: `
      <script>
        import { useStore } from 'vuex'

        export default {
          setup () {
            const store = useStore()
        
            return {
              // access a mutation
              increment: () => store.commit('increment'),
        
              // access an action
              asyncIncrement: () => store.dispatch('asyncIncrement')
            }
          }
        }        
      </script>
    `,
  },
  expected: {
    errors: [],
    warnings: [],
    computed: [],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'increment',
        visibility: 'public',
        description: 'access a mutation',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'increment(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'asyncIncrement',
        visibility: 'public',
        description: 'access an action',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'asyncIncrement(): unknown',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/chat/components/App.vue',
  // only: true,
  options: {
    filename: resolve('classic/chat/components/App.vue'),
    plugins: [
      createVuexPlugin(),
    ],
  },
  expected: {
    name: 'App',
    errors: [],
    warnings: [],
    computed: [],
    data: [],
    props: [],
    methods: [],
  },
});

ComponentTestCase({
  name: 'examples: classic/chat/components/Message.vue',
  // only: true,
  options: {
    filename: resolve('classic/chat/components/Message.vue'),
    plugins: [
      createVuexPlugin(),
    ],
  },
  expected: {
    name: 'Message',
    errors: [],
    warnings: [],
    computed: [],
    data: [],
    props: [
      {
        kind: 'prop',
        name: 'message',
        type: 'object',
        visibility: 'public',
        required: false,
        describeModel: false,
        keywords: [],
      },
    ],
    methods: [
      {
        kind: 'method',
        name: 'time',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'value',
            type: 'unknown',
            rest: false,
          },
        ],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'time(value: unknown): unknown',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/chat/components/MessageSection.vue',
  // only: true,
  options: {
    filename: resolve('classic/chat/components/MessageSection.vue'),
    plugins: [
      createVuexPlugin(),
    ],
  },
  expected: {
    name: 'MessageSection',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'thread',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'messages',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [
      {
        kind: 'data',
        name: 'text',
        type: 'string',
        visibility: 'public',
        initialValue: '""',
        keywords: [],
      },
    ],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'sendMessage',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'dispatch',
            type: 'unknown',
            rest: false,
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'sendMessage(dispatch: unknown): void',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/chat/components/Thread.vue',
  // only: true,
  options: {
    filename: resolve('classic/chat/components/Thread.vue'),
    plugins: [
      createVuexPlugin(),
    ],
  },
  expected: {
    name: 'Thread',
    errors: [],
    warnings: [],
    computed: [],
    data: [],
    props: [
      {
        kind: 'prop',
        name: 'thread',
        type: 'object',
        visibility: 'public',
        required: false,
        describeModel: false,
        keywords: [],
      },
      {
        kind: 'prop',
        name: 'active',
        type: 'boolean',
        visibility: 'public',
        required: false,
        describeModel: false,
        keywords: [],
      },
    ],
    methods: [
      {
        kind: 'method',
        name: 'time',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'value',
            type: 'unknown',
            rest: false,
          },
        ],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'time(value: unknown): unknown',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/chat/components/ThreadSection.vue',
  // only: true,
  options: {
    filename: resolve('classic/chat/components/ThreadSection.vue'),
    plugins: [
      createVuexPlugin(),
    ],
  },
  expected: {
    name: 'ThreadSection',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'threads',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'currentThread',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'unreadCount',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'switchThread',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'switchThread(): unknown',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/chat/components/ThreadSection.vue with resource file',
  // only: true,
  options: {
    filename: resolve('classic/chat/components/ThreadSection.vue'),
    plugins: [
      createVuexPlugin([
        resolve('classic/chat/store/index.js'),
      ]),
    ],
  },
  expected: {
    name: 'ThreadSection',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'threads',
        type: 'object',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'currentThread',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'unreadCount',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'switchThread',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'payload',
            type: 'unknown',
            rest: false,
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'switchThread(payload: unknown): void',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/counter/Counter.vue',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filename: resolve('classic/counter/Counter.vue'),
  },
  expected: {
    name: 'Counter',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'evenOrOdd',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'increment',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'increment(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'decrement',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'decrement(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'incrementIfOdd',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'incrementIfOdd(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'incrementAsync',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'incrementAsync(): unknown',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/counter/Counter.vue with resource file',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin([
        resolve('classic/counter/store.js'),
      ]),
    ],
    filename: resolve('classic/counter/Counter.vue'),
  },
  expected: {
    name: 'Counter',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'evenOrOdd',
        type: 'string',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'increment',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'increment(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'decrement',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'decrement(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'incrementIfOdd',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'incrementIfOdd(): void',
        ],
      },
      {
        kind: 'method',
        name: 'incrementAsync',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'Promise',
          description: undefined,
        },
        syntax: [
          'incrementAsync(): Promise',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/counter-hot/CounterControls.vue',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filename: resolve('classic/counter-hot/CounterControls.vue'),
  },
  expected: {
    name: 'CounterControls',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'count',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'recentHistory',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'increment',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'increment(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'decrement',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'decrement(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'incrementIfOdd',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'incrementIfOdd(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'incrementAsync',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'incrementAsync(): unknown',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/counter-hot/CounterControls.vue with resource file',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin([
        resolve('classic/counter-hot/store/index.js'),
      ]),
    ],
    filename: resolve('classic/counter-hot/CounterControls.vue'),
  },
  expected: {
    name: 'CounterControls',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'count',
        type: 'number',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'recentHistory',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'increment',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'increment(): void',
        ],
      },
      {
        kind: 'method',
        name: 'decrement',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'decrement(): void',
        ],
      },
      {
        kind: 'method',
        name: 'incrementIfOdd',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'incrementIfOdd(): void',
        ],
      },
      {
        kind: 'method',
        name: 'incrementAsync',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'incrementAsync(): void',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/shopping-cart/components/App.vue',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filename: resolve('classic/shopping-cart/components/App.vue'),
  },
  expected: {
    name: 'App',
    errors: [],
    warnings: [],
    computed: [],
    data: [],
    props: [],
    methods: [],
  },
});

ComponentTestCase({
  name: 'examples: classic/shopping-cart/components/ProductList.vue',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filename: resolve('classic/shopping-cart/components/ProductList.vue'),
  },
  expected: {
    name: 'ProductList',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'products',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'addProductToCart',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'addProductToCart(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'currency',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'currency(): unknown',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/shopping-cart/components/ProductList.vue with resource file',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin([
        resolve('classic/shopping-cart/store/index.js'),
      ]),
    ],
    filename: resolve('classic/shopping-cart/components/ProductList.vue'),
  },
  expected: {
    name: 'ProductList',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'products',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'addProductToCart',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'product',
            type: 'unknown',
            rest: false,
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'addProductToCart(product: unknown): void',
        ],
      },
      {
        kind: 'method',
        name: 'currency',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'currency(): unknown',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/shopping-cart/components/ShoppingCart.vue',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filename: resolve('classic/shopping-cart/components/ShoppingCart.vue'),
  },
  expected: {
    name: 'ShoppingCart',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'checkoutStatus',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'products',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'total',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'currency',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'currency(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'checkout',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'products',
            rest: false,
            type: 'unknown',
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'checkout(products: unknown): void',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/shopping-cart/components/ShoppingCart.vue with resource file',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin([
        resolve('classic/shopping-cart/store/index.js'),
      ]),
    ],
    filename: resolve('classic/shopping-cart/components/ShoppingCart.vue'),
  },
  expected: {
    name: 'ShoppingCart',
    errors: [],
    warnings: [],
    computed: [
      {
        kind: 'computed',
        name: 'checkoutStatus',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'products',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
      {
        kind: 'computed',
        name: 'total',
        type: 'unknown',
        visibility: 'public',
        keywords: [],
        dependencies: [],
      },
    ],
    data: [],
    props: [],
    methods: [
      {
        kind: 'method',
        name: 'currency',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'currency(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'checkout',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'products',
            rest: false,
            type: 'unknown',
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'checkout(products: unknown): void',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/todomvc/components/TodoItem.vue',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin(),
    ],
    filename: resolve('classic/todomvc/components/TodoItem.vue'),
  },
  expected: {
    name: 'Todo',
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'editing',
        type: 'boolean',
        keywords: [],
        initialValue: 'false',
        visibility: 'public',
      },
    ],
    props: [
      {
        kind: 'prop',
        name: 'todo',
        type: 'unknown',
        describeModel: false,
        required: true,
        keywords: [],
        default: undefined,
        visibility: 'public',
      },
    ],
    methods: [
      {
        kind: 'method',
        name: 'editTodo',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'editTodo(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'toggleTodo',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'toggleTodo(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'removeTodo',
        visibility: 'public',
        keywords: [],
        params: [],
        returns: {
          type: 'unknown',
          description: undefined,
        },
        syntax: [
          'removeTodo(): unknown',
        ],
      },
      {
        kind: 'method',
        name: 'doneEdit',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'e',
            rest: false,
            type: 'unknown',
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'doneEdit(e: unknown): void',
        ],
      },
      {
        kind: 'method',
        name: 'cancelEdit',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'e',
            rest: false,
            type: 'unknown',
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'cancelEdit(e: unknown): void',
        ],
      },
    ],
  },
});

ComponentTestCase({
  name: 'examples: classic/todomvc/components/TodoItem.vue with resource file',
  // only: true,
  options: {
    plugins: [
      createVuexPlugin([
        resolve('classic/todomvc/store/index.js'),
      ]),
    ],
    filename: resolve('classic/todomvc/components/TodoItem.vue'),
  },
  expected: {
    name: 'Todo',
    errors: [],
    warnings: [],
    computed: [],
    data: [
      {
        kind: 'data',
        name: 'editing',
        type: 'boolean',
        keywords: [],
        initialValue: 'false',
        visibility: 'public',
      },
    ],
    props: [
      {
        kind: 'prop',
        name: 'todo',
        type: 'unknown',
        describeModel: false,
        required: true,
        keywords: [],
        default: undefined,
        visibility: 'public',
      },
    ],
    methods: [
      {
        kind: 'method',
        name: 'editTodo',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: '{ todo, value }',
            rest: false,
            type: 'unknown',
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'editTodo({ todo, value }: unknown): void',
        ],
      },
      {
        kind: 'method',
        name: 'toggleTodo',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'todo',
            rest: false,
            type: 'unknown',
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'toggleTodo(todo: unknown): void',
        ],
      },
      {
        kind: 'method',
        name: 'removeTodo',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'todo',
            rest: false,
            type: 'unknown',
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'removeTodo(todo: unknown): void',
        ],
      },
      {
        kind: 'method',
        name: 'doneEdit',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'e',
            rest: false,
            type: 'unknown',
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'doneEdit(e: unknown): void',
        ],
      },
      {
        kind: 'method',
        name: 'cancelEdit',
        visibility: 'public',
        keywords: [],
        params: [
          {
            name: 'e',
            rest: false,
            type: 'unknown',
          },
        ],
        returns: {
          type: 'void',
          description: undefined,
        },
        syntax: [
          'cancelEdit(e: unknown): void',
        ],
      },
    ],
  },
});
