export const count = statex => statex.count

export const recentHistory = state => {
  return state.history
    .slice(-5)
    .join(', ')
}
